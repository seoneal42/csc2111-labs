#include "ArrayList.h"
#include <list>
#include <iostream>
#include <string>

void ArrayListImpl();
void ListImpl();
void NewMethodsImpl();
void ArrayListAddEmpty();

int main(){
      ArrayListAddEmpty();
      std::cout << "\n\n";
      ArrayListImpl();
      std::cout << "\n\n";
      ListImpl();
      std::cout << "\n\n";
      NewMethodsImpl();
      
      return 0;
}

void ArrayListAddEmpty(){
      std::cout << "Array List Add Empty Implementation: " << std::endl << std::endl;
      ArrayList<std::string>* myImpl = new ArrayList<std::string>();

      std::string* string1 = NULL;

      bool added = myImpl->add(0, string1);

      if(!added)
            std::cout << "Added empty string to array list: false" << std::endl;
      else
            std::cout << "Added empty string to array list: true" << std::endl;

      delete myImpl;
}

void ArrayListImpl(){
      std::cout << "Array List Implementation: " << std::endl << std::endl;
      ArrayList<string>* myImpl = new ArrayList<string>();
      
      std::string* bread = new std::string("bread");
      myImpl->append(bread);
      std::string* cookies = new std::string("cookies");
      myImpl->append(cookies);
      std::string* cake = new std::string("cake");
      myImpl->append(cake);
      std::string* bananas = new std::string("bananas");
      myImpl->append(bananas);
      std::string* parsley = new std::string("parsley");
      myImpl->append(parsley);
      
      if(myImpl->remove(1))
            std::cout << "Removed cookies from the list." << std::endl;
            
      std::string* apples = new std::string("apples");
      if(myImpl->add(3, apples))
            std::cout << "Added apples between bananas and parsley" << std::endl;
            
      ArrayList<string>* myImplReversed = new ArrayList<string>();
      
      int length = myImpl->size();
      for(int i = length; i > 0; i--){
            myImplReversed->append(myImpl->get(i-1));
      }
      
      std::string* swap = myImplReversed->remove(0);
      std::string* swap2 = myImplReversed->remove(0);
      
      myImplReversed->add(0, swap);
      myImplReversed->add(0, swap2);
      
      length = myImplReversed->size();
      for(int i = 0; i < length; i++)
            std::cout << *myImplReversed->get(i) << ", ";
            
      std::cout << std::endl;
      
      delete myImpl;
      delete myImplReversed;
      
      delete bread;
      delete apples;
      delete cookies;
      delete cake;
      delete bananas;
      delete parsley;
}

void ListImpl(){
      std::cout << "List Implementation: " << std::endl << std::endl;
      list<std::string>* myImpl = new list<std::string>();
      
      std::string* bread = new std::string("bread");
      myImpl->push_back(*bread);
      std::string* cookies = new std::string("cookies");
      myImpl->push_back(*cookies);
      std::string* cake = new std::string("cake");
      myImpl->push_back(*cake);
      std::string* bananas = new std::string("bananas");
      myImpl->push_back(*bananas);
      std::string* parsley = new std::string("parsley");
      myImpl->push_back(*parsley);
       std::string* apples = new std::string("apples");
      
      myImpl->remove(*cookies);
      
      list<std::string>::iterator it = myImpl->begin();
      
      while(*it != *parsley)
            it++;
            
      myImpl->insert(it, *apples);
      
      myImpl->reverse();
      
      it = myImpl->begin();
      std::string swap = *it;
      it++;
      std::string swap2 = *it;
      
      myImpl->remove(swap);
      myImpl->remove(swap2);
      
      myImpl->insert(myImpl->begin(), swap);
      myImpl->insert(myImpl->begin(), swap2);
      
      for(it = myImpl->begin(); it != myImpl->end(); it++){
            std::cout << *it << ", ";
      }
      
      std::cout << std::endl;
      
      
      delete myImpl;
      delete bread;
      delete apples;
      delete cookies;
      delete cake;
      delete bananas;
      delete parsley;
}

void NewMethodsImpl(){
      std::cout << "Newly Created Methods of ArrayList (isEmpty, swap, append): " << std::endl << std::endl;
      
      ArrayList<int> *myImpl = new ArrayList<int>();
      
      std::cout << "Testing Empty before adding: ";
      if(myImpl->isEmpty())
            std::cout << "true" << std::endl;
      else
            std::cout << "false" << std::endl;
      
      int* myInt1 = new int(1);
      int* myInt2 = new int(3);
      int* myInt3 = new int(5);
      int* myInt4  = new int(7);
      
      std::cout << "Testing appending" << std::endl;
      if(myImpl->append(myInt1))
            std::cout << "Successfully added: " << *myInt1 << std::endl;
      if(myImpl->append(myInt2))
            std::cout << "Successfully added: " << *myInt2 << std::endl;
      if(myImpl->append(myInt3))
            std::cout << "Successfully added: " << *myInt3 << std::endl;
      if(myImpl->append(myInt4))
            std::cout << "Successfully added: " << *myInt4 << std::endl;
            
      std::cout << "Testing Empty after adding: ";
      if(myImpl->isEmpty())
            std::cout << "true" << std::endl;
      else
            std::cout << "false" << std::endl;
            
      std::cout << "Testing subList: " << std::endl;
      
      ArrayList<int>* subMyImpl = myImpl->subList(2, 4);
     
      int length = myImpl->size();
      std::cout << "Original List: ";
      for(int i = 0; i < length; i++)
            std::cout << *myImpl->get(i) << " ";
      
      length = subMyImpl->size();
      std::cout << "SubList: ";
      for(int i = 0; i < length; i++)
            std::cout << *subMyImpl->get(i) << " ";
            
      delete myInt1;
      delete myInt2;
      delete myInt3;
      delete myInt4;
      delete myImpl;
      delete subMyImpl;
}