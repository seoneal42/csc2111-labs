#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/Walker.h"
#include "../src/Graph.h"
#include "../src/ArrayList.h"
#include "../src/Vertex.h"

TEST(Walker, DepthandBreadthSearchWith6Elements) {
	/*List of Vertexes*/
	Vertex v1(2);
	Vertex v2(3);
	Vertex v3(10);
	Vertex v4(6);
	Vertex v5(23);
	Vertex v6(18);

	Graph gph;
	gph.addVertex(&v1);
	gph.addVertex(&v2);
	gph.addVertex(&v3);
	gph.addVertex(&v4);
	gph.addVertex(&v5);
	gph.addVertex(&v6);

	//Add edges
	gph.addEdge(&v1, &v5);
	gph.addEdge(&v1, &v4);
	gph.addEdge(&v1, &v3);
	gph.addEdge(&v2, &v4);
	gph.addEdge(&v2, &v5);
	gph.addEdge(&v2, &v3);
	gph.addEdge(&v3, &v6);

	//Walk through each one with breadth and depth
	Walker walk;
	ArrayList<Vertex>* depthSearch = walk.depthFirstSearch(&v1);

	EXPECT_EQ(depthSearch->get(0)->getValue(), v1.getValue());
	EXPECT_EQ(depthSearch->get(5)->getValue(), v5.getValue());

	//Reset all to false
	for(int i = 0; i < depthSearch->size(); i++){
		depthSearch->get(i)->setVisited(false);
	}

	ArrayList<Vertex>* breadthSearch = walk.breadthFirstSearch(&v1);


	EXPECT_EQ(breadthSearch->get(0)->getValue(), v1.getValue());
	EXPECT_EQ(breadthSearch->get(5)->getValue(), v6.getValue());
}

TEST(Walker, DepthandBreadthSearchWith0Elements){
	Walker walk;

	ArrayList<Vertex>* depthSearch = walk.depthFirstSearch(NULL);

	bool isNull;

	if(depthSearch == NULL)
		isNull = true;

	EXPECT_TRUE(isNull);

	ArrayList<Vertex>* breadthSearch = walk.breadthFirstSearch(NULL);

	if(breadthSearch == NULL)
		isNull = true;

	EXPECT_TRUE(isNull);
}

TEST(Walker, DepthandBreadthSearchWith2Elment){
	Vertex v1(3);
	Vertex v2(7);
	Graph gph;

	gph.addVertex(&v1);
	gph.addVertex(&v2);

	gph.addEdge(&v1, &v2);

	Walker walk;

	ArrayList<Vertex>* depthSearch = walk.depthFirstSearch(&v1);

	EXPECT_EQ(depthSearch->get(0)->getValue(), v1.getValue());
	EXPECT_EQ(depthSearch->size(), 2);

	for(int i = 0; i < depthSearch->size(); i++){
		depthSearch->get(i)->setVisited(false);
	}
	ArrayList<Vertex>* breadthSearch = walk.breadthFirstSearch(&v1);

	EXPECT_EQ(breadthSearch->get(0)->getValue(), v1.getValue());
	EXPECT_EQ(breadthSearch->size(), 2);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
