cmake_minimum_required(VERSION 2.8)
project(Lab13 C CXX)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

if(CMAKE_COMPILER_IS_CNUCXX)
    add_definitions(-Wall -ansi -Wno-deprecated -pthread -std=gnu++11 -lgtest)
endif()

if(MSVC)
    set(MSVC_COMPILER_DEFS "-D_VARIADIC_MAX=10")
endif()

enable_testing()

add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(libcommon)
add_subdirectory(libbst)